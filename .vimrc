" Set color scheme 
color desert

" Lots of undo levels, just in case
set undolevels=5000

" Tabs and indenting
set softtabstop=2
set shiftwidth=2
set expandtab
set autoindent
set smarttab

" Map df to Esc in Insert mode
imap df <Esc>

" Cd to home at start
cd ~

syntax on

" Displays a line at 80 columns
set colorcolumn=80
highlight ColorColumn ctermbg=black guibg=black
"====[ Toggle visibility of naughty characters ]============

" Make naughty characters visible...
" (uBB is right double angle, uB7 is middle dot)
set lcs=tab:»·,trail:␣,nbsp:˷
highlight InvisibleSpaces ctermfg=Black ctermbg=Black
call matchadd('InvisibleSpaces', '\S\@<=\s\+\%#\ze\s*$', -10)

augroup VisibleNaughtiness
    autocmd!
    autocmd BufEnter  *       set list
    autocmd BufEnter  *       set list
    autocmd BufEnter  *.txt   set nolist
    autocmd BufEnter  *.vp*   set nolist
    autocmd BufEnter  *       if !&modifiable
    autocmd BufEnter  *           set nolist
    autocmd BufEnter  *       endif
augroup END
